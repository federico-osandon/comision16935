import { memo } from 'react'
import Item from './Item'


//     memo(()=>{})       memo(()=>{}, fn)

const ItemList = memo(
    ({personas}) => {
    
        console.log('soy item list')
        return (
            <>
               {personas.map(persona => <Item persona={persona} />  )}
            </>
        )
    }
)
    
    
    export default ItemList

