import {useState, useEffect} from 'react'
import { useParams } from 'react-router';
import { getFirestore } from '../../services/getFirebase';

import { getFetchUno } from '../../utils/Mock';
import ItemDetail from '../ItemDetail/ItemDetail';



const ItemDetailContainer = () => {
    const [item, setItem] = useState({})
    const {id} = useParams()

    useEffect(() => {
        
        const dbQuey = getFirestore ()
        dbQuey.collection('items').doc(id).get()
        .then(resp =>  setItem( { id , ...resp.data() } ))
        .catch(err=>console.log(err))
        //.finally(()=> setLoading(false))
    
    }, [id])
   
    
    return (
        <div>
            <ItemDetail item={item} />
        </div>
    )
}

export default ItemDetailContainer
