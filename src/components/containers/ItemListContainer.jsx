import { useState, useEffect } from 'react'
import { useParams } from "react-router-dom";
import { getFirestore } from '../../services/getFirebase';
import { getFetch } from '../../utils/Mock'
import ItemList from '../ItemList/ItemList'



console.log(getFetch)

function ItemListContainer({greeting}) { 
    const [personas, setPersonas] = useState([])
    const [loading, setLoading] = useState(true)
    const [bool, setBool] = useState(true)
    const { idCategoria } = useParams()

    

    useEffect(() => {      
       
        const dbQuey = getFirestore()
        const newQuery = idCategoria ? 
                                dbQuey.collection('items').where('categoryID', '==', idCategoria)  
                            : 
                                dbQuey.collection('items')         

        newQuery.get()
        .then(resp => {
            setPersonas( resp.docs.map(persona => ( {id: persona.id, ...persona.data() } )) )
        } )
        .catch(err=> console.log(err))
        .finally(()=> setLoading(false))                
        
    }, [idCategoria])   
    

    return (
        <div>           
            
                       
            <h1> {greeting}</h1>          
            { loading ? <h2>Cargando...</h2> :   <ItemList personas={personas} />  }       
            
          
        </div>
    )
}

export default ItemListContainer
