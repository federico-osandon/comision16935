

const estados= ['js', 'React js', 'Component']


const Item = ({valor}) => {
    return (
        <button className="btn btn-outline-primary p-3">{valor}</button>
    )
}


function ComponenteConteiner() {

    const items = estados.map(t => <Item valor={t}/>)
    //llamada a una api
    console.log(items)
    return (
        <div>
            {items}
        </div>
    )
}

export default ComponenteConteiner
