import { useCartContext } from '../../context/cartContext'
import ItemCount from '../ItemCount'


const ItemDetail = ( {item} ) => {    
    
    const { addToCart } = useCartContext() 
    
    const onAdd=(cant)=>{       
        addToCart( item, cant)
    }   
   
    return (
        <>
            <h2>{item.id}</h2>  
            <h2>{item.title}</h2>  
            <h2>{item.price}</h2> 
            <img src={item.imageID} alt='' /> 

            <ItemCount initial={1} stock={5} onAdd={onAdd} /> 
          

        </>
    )
}

export default ItemDetail
