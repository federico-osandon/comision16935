import {useEffect, useState} from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'
import { getFetch } from './utils/Mock'



const ComponentePromesas = () => {
    const [personas, setPersonas] = useState([])
    const [loading, setLoading] = useState(true)
    
    
   
    useEffect(() => {
        getFetch
        .then(res => {
            setPersonas(res)
            setLoading(false)
        })
          
       
    }, [])
        
  //console.log(personas)

    return (
        <>
           <h2>Soy componentePromesas</h2> 
        
                   
             
        </>
    )
}

export default ComponentePromesas
