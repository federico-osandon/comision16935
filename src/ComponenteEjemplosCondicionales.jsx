import { useState, useEffect } from "react";


 
export const ControlledInput = () => {

    const [input, setInput] = useState("");

    console.log(input);
    console.log('nuevo renderizado');
    return (
      <input
        type="text"
        value={input}
        onInput={(evet) => setInput(evet.target.value)}
      />
    );
  };


  
export  function LoadingComponent() {
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {

      setTimeout(() => {
        setLoading(false);
      }, 5000);
      return ()=>{
          console.log('Limpiando componente');
      }
    }, []);
    
    return <>
        {loading ? <h2>Loading... </h2> : <h3>Productos cargardos!</h3>}
    </>;
  }


  
export  function TextComponent({ condition = false }) {
    
    if (condition) {
      return <h2>Uds no esta logeado</h2>;
    }
  
    return (
      <>
        <h2>Ud esta logueado puede ver la pág.</h2>
      </>
    )
  }



export  function TextComponent2({ condition = false }) {
    //  ? : si no
    // && if sin el else
    // ||
    // ! negación
    return (
      <>
        {condition && <h2>Ud esta logueado puede ver la pág.</h2> }

        { !condition && <h2>Ud no esta logueado, NO puede ver la pág.</h2>}

      </>
    );
  }



export  function TextComponent3({ condition = true }) {
    return (
      <>
        <h2>
          {condition ? 'Ud No esta logueado puede ver la pág.' : 'Ud esta logueado puede ver la pág.'}
        </h2>                    
      </>
    )
  }






    export function TextComponent4({ condition = false }) {

    return (
        <>
            <h2 
                style={ { color: condition ? "green" : "red" } }
            >
                Ud esta logueado puede ver la pág.
            </h2>

            
        </>
    );
    }








  
export  function TextComponent5({ condition = true }) {
    return (
      <>
        <h2 className={ (condition === true) ? "btn btn-success" : "btn btn-danger" }>
          Ud esta logueado puede ver la pág.
        </h2>
      </>
    );
  }



  



export  function TextComponent6({ condition = false, otro = "mt-5" }) {
    return (
      <>
        <h2
          className={ `${condition === true ? "btn btn-success" : "btn btn-danger"} ${otro || ""} `}
        >
          Ud esta logueado puede ver la pág.
        </h2>
      </>
    );
  }




export function TextComponent7({ condition = true , otro = "mt-5" }) {
    
    const config = condition
    
      ? {
          className: `btn btn-success ${otro || ""}`,
          style: {color: 'red'},
          title: "Este es el titulo si la condicion es verdadera",
          nombre: 'Fede'
        }
      : 
        {}
    
      return (
      <>
        <h2 {...config} >Ud esta logueado puede ver la pág.</h2>
      </>
    )
  }