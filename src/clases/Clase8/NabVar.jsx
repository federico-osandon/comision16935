import CartWidget from "./CartWidget"
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'

import { BsFillArchiveFill } from 'react-icons/bs'
import { Link } from "react-router-dom"

//import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap'


export const NabVar=(props)=>{
    const {count} = props
    return(
        <>  
            <Navbar bg="light" expand="lg">
                <Container>
                    <Link exact to='/' >
                        <Navbar.Brand>React-Bootstrap</Navbar.Brand>
                    </Link>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                    <Link  to='/categoria/adultos' >
                        Adultos                        
                    </Link>
                    <Link  to={`/categoria/jovenes`} >
                        Jóvenes                                             
                    </Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
                    <BsFillArchiveFill />
                    <Link exact to='/cart'>
                        <CartWidget count={count} />
                    </Link>
                </Navbar>         
        </>
    )
} 
