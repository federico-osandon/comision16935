import { useState, useEffect } from 'react'
import { useParams } from "react-router-dom";
import { getFetch } from '../../utils/Mock'

import ItemCount from '../ItemCount'
import ItemList from '../ItemList/ItemList'



console.log(getFetch)

function ItemListContainer({greeting}) { 
    const [personas, setPersonas] = useState([])
    const [loading, setLoading] = useState(true)
    const { idCategoria } = useParams()

    const onAdd=(cant)=>{
        console.log(cant)
    }  

    useEffect(() => {
        if (idCategoria) {
            getFetch
            .then(respuesta =>{
                setPersonas(   respuesta.filter(prod => prod.cateria === idCategoria)   )         
            })
            .catch(error => console.log(error))
            .finally(()=> setLoading(false))             
        }else{
            getFetch
            .then(respuesta =>{
                setPersonas(respuesta)         
            })
            .catch(error => console.log(error))
            .finally(()=> setLoading(false)) 
        }
        
    }, [idCategoria])
    
    console.log(idCategoria);
    return (
        <div>             
            <h1> {greeting}</h1> 

            { loading ? <h2>Cargando...</h2> :   <ItemList personas={personas} />  }  


            <ItemCount stock={5} initial={1} onAdd={onAdd} />
          
        </div>
    )
}

export default ItemListContainer
