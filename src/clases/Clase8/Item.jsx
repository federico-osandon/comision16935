import { Link } from "react-router-dom";

const Item = ({persona}) => {
    return (       
        <div key={persona.id} className='card w-50 mt-3'>
        <div className='card-header'>{persona.name}</div>
        <div className="card-body">
            <img src={persona.foto} alt='foto' />
        </div>               
        <div className="card-footer">
            <Link to={`/detalle/${persona.id}`}>
                <button className="btn/detalle/${} btn-outline-primary btn-block">Detalles</button>
            </Link>
        </div>
    </div>
        
    )
}

export default Item
