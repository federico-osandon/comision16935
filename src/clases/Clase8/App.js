import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { NabVar } from './components/NavBar/NabVar';
import Cart from './components/Cart/Cart';
import ItemListContainer from './components/containers/ItemListContainer';
import ItemDetailContainer from './components/containers/ItemDetailContainer';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Error from "./components/Error";


function App() {

    return (
        <Router>
            <div className="App">
                <NabVar />
                <Switch>
                    <Route exact path='/'  >
                        <ItemListContainer greeting={'hola soy item list container'} />  
                    </Route>

                    <Route path='/categoria/:idCategoria' component={ItemListContainer} />

                    <Route exact path='/detalle/:id' component={ItemDetailContainer} />

                    <Route exact path='/cart' component={Cart} />                    
                    
                </Switch>
            </div>
        </Router>
    );
}

export default App;