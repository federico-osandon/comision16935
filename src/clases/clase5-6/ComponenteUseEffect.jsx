
import { useState, useEffect } from 'react';
import './App.css';



function ComponenteUseEffect({defaultName}) {
    const [count, setCount] = useState(0)
    // const state = 'esto es un estado que morira al finalizar la funcíon'
    const [click, setClick] = useState(false)
    const [click2, setClick2] = useState(false)

    let name = 'soy name'

    function handleClick() {
        setClick(!click)
        // console.log('click');
    }
    
    function handleClick2() {
        setClick2(!click2)
        // console.log('click2');
    }

    // const llamadaApi = () => {
    //     alert('LLamada a una api')
    // }


       const handleCount =()=>{
        setCount(count+1)
        console.log(Date())
       }



    
    // console.log(name);
    // alert('hola')
    
    useEffect(()=>{
        console.log('despues de renderizado efecto 1')
        // console.log('llamada a api');       
    })

    useEffect(()=>{
        console.log('despues de renderizado efecto 2')
        console.log('llamada a api');       
    }, [])//ejecutate una sola vez
    
    useEffect(()=>{
        console.log('despues de renderizado clik')
        console.log('llamada a api');   
        //addEventListener()
        return ()=>{
            //removeEvnetListener
            console.log('limpieza')
        }    
    }, [click])//ejecutate cada cambio de estado
    


    console.log('montado antes de renderizar');

    return (                     
        <div >
            <p>{name}</p>
            <p>{count}</p>
            <button onClick={handleCount}>Click del contador</button>
            <button onClick={handleClick}>Click uno</button>  
            <button onClick={handleClick2}>Click dos</button>  
        </div>    
    );
}

export default ComponenteUseEffect;

// Este es el app que usamos para el ejemplo de la clases


// useEffect(()=>{//ejcucion despues de cada rendering
//     console.log('app montada despues del rendering')
//     llamadaApi()
//   })

//   useEffect(()=>{//componentDidMount(){} solo una vez despues del primero
//     console.log('se ejecuta una sola vez')
//     llamadaApi()
//   }, [])

//   useEffect(()=>{//componentDidMount(){} solo una vez despues del primero
//     console.log('cambio de boleano')
//     //document.addEventListener()
//     llamadaApi()
    
//   }, [])