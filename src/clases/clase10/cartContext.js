import {useState, createContext, useContext } from 'react'

const cartContext= createContext([])

export const useCartContext = () => useContext(cartContext) 


export default function CartContextProvider ({children}) {
    const [carList, setCarList] = useState([])

    function addToCart(item) {
        setCarList([...carList, item])
    }

    function borrarLista() {
        carList([])
    }

    console.log(carList)
    return(
        <cartContext.Provider value={{
            carList,
            addToCart,
            borrarLista
        }}>
            {children}
        </cartContext.Provider>
    )
}