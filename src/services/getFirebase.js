import firebase from "firebase"

import 'firebase/firestore'


const firebaseConfig = {
    apiKey: process.env.APIKEY,
    authDomain: "comision16935.firebaseapp.com",
    projectId: "comision16935",
    storageBucket: "comision16935.appspot.com",
    messagingSenderId: "731029161030",
    appId: "1:731029161030:web:2e1b63c7d101edad22b647"
};


const app = firebase.initializeApp(firebaseConfig)


// export function getFirebase(){
//     return app
// }

export function getFirestore(){    
    return firebase.firestore(app)
}


