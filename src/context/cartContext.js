import {useState, createContext, useContext } from 'react'

const cartContext= createContext([])

export const useCartContext = () => useContext(cartContext) 


export default function CartContextProvider ({children}) {
    const [carList, setCarList] = useState([])

    // function addToCart(item) {
    //     setCarList([...carList, item])
    // }

    // const addToCart = (data) => {
    //     let previousCart = [...carList]

    //     if (previousCart.some(i => i.item.id === data.item.id)) {
            
    //         previousCart.find(i => i.item.id === data.item.id).cantidad +=  data.cantidad
    //         setCarList(previousCart)
    //     } else {
    //         setCarList( [...carList, data ] ) //simil Push
    //     }
    // }

    const addToCart = (item, quantity) => {

        const index = carList.findIndex(i => i.item.id === item.id)//-1, pos
        console.log(index)
    
          if (index > -1) {
            const oldQy = carList[index].quantity
    
            carList.splice(index, 1)
            setCarList([...carList, { item, quantity: quantity + oldQy}])
          }
          else {
            setCarList([...carList, {item, quantity}])
          }
      }

      const deleteFromCart = (item) => {
            //Verificamos si esta en el carrito  

            const deleteProduct = carList.filter((prod) => prod.item.id !== item.item.id);
        
            setCarList([...deleteProduct]);
        };  


        const iconCart = () => {
            return carList.reduce( (acum, valor)=> acum + valor.quantity, 0) 
            //return product.length
        }


        const precioTotal =()=>{
            return carList.reduce((acum, valor)=>(acum + (valor.quantity * valor.item.price)), 0) 
        }

    function borrarLista() {
        carList([])
    }





    console.log(carList);
    return(
        <cartContext.Provider value={{
            carList,
            addToCart,
            deleteFromCart,
            iconCart,
            precioTotal,
            borrarLista
        }}>
            {children}
        </cartContext.Provider>
    )
}





//   const deleteFromCart = (item) => {
//     //Verificamos si esta en el carrito   
//     const deleteProduct = product.filter((prod) => prod.item.id !== item.item.id);

//     setProducts([...deleteProduct]);
//   };   
  
//   //[1,2,3,4] Acum= 0 => 1, 1+2 =>  3+3

//   const iconCart = () => {
//     return product.reduce( (acum, valor)=> acum + valor.quantity, 0) 
//     //return product.length
//   }


//   const borrarListado=()=>{
//       setProducts([])
//   }       

  

//   const precioTotal =()=>{
//     return product.reduce((acum, valor)=>(acum + (valor.quantity * valor.item.price)), 0) 
//   }

 

//   // const clearCart = () => setProducts([]);
 
 
//   console.log(product)